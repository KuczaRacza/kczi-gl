#pragma once
#include "renderer.hpp"
#include "scissors.hpp"
#include "scoped_bind.hpp"
#include "texture.hpp"
#include "pch.hpp"
namespace kczi {

class Framebuffer {
public:
	explicit Framebuffer(Renderer &renderer);
	Framebuffer(Framebuffer &) = delete;
	Framebuffer(Framebuffer &&) noexcept;
	explicit Framebuffer(u2_32 size, Renderer &renderer);
	~Framebuffer();
	auto operator=(Framebuffer &&) noexcept -> Framebuffer &;
	auto operator=(Framebuffer &) -> Framebuffer & = delete;
	auto create(u2_32 m_size) -> void;
	auto bind() const noexcept -> void;
	static auto unbind() noexcept -> void;
	auto clear() const noexcept -> void;
	Texture texture{};
	u2_32 size{};
	u32 handle{};
	Renderer &renderer;
	Viewport fb_viewport;
};
template <ScopedBindable T>
class ScopedFramebuffer {
public:
	ScopedBind<Framebuffer, T> fb_bound;
	ScopedBind<Viewport, T> viewport_bound;
	ScopedFramebuffer(Framebuffer &fb, T &t) : fb_bound(fb, t), viewport_bound(fb.fb_viewport, t) {}
};

} // namespace kczi
