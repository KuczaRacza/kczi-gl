#include "matrix.hpp"
#include <glm/ext/matrix_clip_space.hpp>
#include <glm/ext/matrix_float4x4.hpp>
#include <glm/ext/matrix_transform.hpp>
#include <glm/fwd.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>
#include <types.hpp>
namespace kczi {
Matrix::Matrix() {
	auto projection = glm::identity<glm::mat4>();
	memcpy(matrix_data.data(), glm::value_ptr(projection), sizeof(float) * 16);
}
Matrix::Matrix(f2_32 end, f2_32 start) {
	auto projection = glm::ortho(start.x, end.x, end.y, start.y, 0.0f, 10.0f);
	memcpy(matrix_data.data(), glm::value_ptr(projection), sizeof(float) * 16);
}
} // namespace kczi