#pragma once
#include "expected.hpp"
#include "matrix.hpp"
#include <GL/glew.h>
#include <algorithm>
#include <initializer_list>
#include <stdexcept>
#include <string>
#include <string_view>
#include <tuple>
#include <types.hpp>

namespace kczi {

class GlProgramError  {
public:
	u8 err{};
	std::string compile_error;
	[[nodiscard]] auto what() const noexcept -> std::string_view;
	inline auto operator==(const GlProgramError &other) const noexcept -> bool;
};
class ShaderRaw {
public:
	u32 handle_id = 0;
	ShaderRaw(ShaderRaw &) = delete;
	ShaderRaw(ShaderRaw &&other) noexcept;
	ShaderRaw();
	~ShaderRaw();
	auto bind() const -> void;
};
template <typename T>
concept UniformHolder = requires(T a) {{ T::has_texture() } -> std::same_as<bool>; };
template <typename T>
struct UniformLocation {
	u32 location = 0;
};
template <typename T>
struct UniformPair {
	UniformLocation<T> &loc;
	std::string_view name;
	UniformPair(std::string_view name, UniformLocation<T> &loc) : loc(loc), name(name) {}
};
template <UniformHolder T>
class Shader {
public:
	Shader(ShaderRaw &&shader);
	Shader(Shader &&shader) noexcept = default;
	Shader(const std::string &fs_source, const std::string &vs_source);
	ShaderRaw raw;
	template <typename... Args>
	inline auto get_uniform_locations(Args &&...args) -> void;
	inline auto set_uniform(const UniformLocation<Matrix> &loc, const Matrix &matrix);
	inline auto set_uniform(const UniformLocation<float> &loc, const float &value);

	T shader_cache{};
	inline auto bind() -> void;
};
auto create_program(const std::string &fs_source, const std::string &vs_source)
		-> ShaderRaw;

// impl
inline auto
GlProgramError::operator==(const GlProgramError &other) const noexcept -> bool {
	return err ==  other.err;
}
template <UniformHolder T>
Shader<T>::Shader(const std::string &fs_source, const std::string &vs_source)
		: raw(std::move(create_program(fs_source, vs_source))) {}
template <UniformHolder T>
inline auto Shader<T>::bind() -> void { raw.bind(); }
template <UniformHolder T>
Shader<T>::Shader(ShaderRaw &&shader) : raw(std::move(shader)) {}
template <typename... Args>
inline auto get_uniform_loc_recur(u32 handle) {}
template <typename T, typename... Args>
inline auto get_uniform_loc_recur(u32 handle, T &&element, Args &&...args)
		-> void {
	element.loc.location = (u32)glGetUniformLocation(handle, element.name.data());
	if (element.loc.location == (u32)-1) [[unlikely]] {
		throw std::runtime_error("uniform not found. maybe opted out?");
	}
	get_uniform_loc_recur(handle, args...);
}
template <UniformHolder T>
template <typename... Args>
inline auto Shader<T>::get_uniform_locations(Args &&...args) -> void {
	get_uniform_loc_recur(raw.handle_id, args...);
}
template <UniformHolder T>
inline auto Shader<T>::set_uniform(const UniformLocation<Matrix> &loc, const Matrix &matrix) {
	bind();
	glUniformMatrix4fv((GLint)loc.location, 1, GL_FALSE, matrix.matrix_data.data());
}
template <UniformHolder T>
inline auto Shader<T>::set_uniform(const UniformLocation<float> &loc, const float &value) {
	bind();
	glUniform1f((GLint)loc.location, value);
}

} // namespace kczi