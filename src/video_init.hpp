#pragma once
#include "expected.hpp"
#include "pch.hpp"
struct SDL_Window;
namespace kczi {
class VideoInitErr {
public:
  u8 err{};
	VideoInitErr() = default;
  explicit VideoInitErr(u8 err);
  [[nodiscard]] auto what() const noexcept -> std::string_view;
  auto operator==(const VideoInitErr &other) const noexcept -> bool;
};
class GlContext {
public:
  class Params {
  public:
    std::string window_name;
    u2_32 window_size = ty2(1600u, 900u);
  };
  SDL_Window *window = nullptr;
  void *sld_gl_context = nullptr;
  GlContext() = default;
  GlContext(GlContext &&other) noexcept ;
  GlContext(GlContext &) = delete;
  ~GlContext();
  auto swap() noexcept -> void;
};
auto init_viedo(const GlContext::Params &params)
    -> Expected<GlContext, VideoInitErr>;
} // namespace kczi