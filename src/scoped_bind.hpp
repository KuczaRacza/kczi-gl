#pragma once
#include <concepts>
namespace kczi {
template <typename T>
concept Bindable = requires(T a) {
										 { a.bind() } -> std::same_as<void>;
										 { a.unbind() } -> std::same_as<void>;
									 };
template <typename T>
concept ScopedBindable = requires(T a) {
													 { a.bind() } -> std::same_as<void>;
													 { a.unbind() } -> std::same_as<void>;
													 { a.is_bound() } -> std::same_as<bool>;
												 };
class DummyBind {
public:
	DummyBind() = default;
	~DummyBind() = default;
	auto bind() const noexcept -> void {};
	auto unbind() const noexcept -> void {};
	[[nodiscard]] static auto is_bound() noexcept -> bool { return false; };
};
template <Bindable T, ScopedBindable Y>
class ScopedBind {
public:
	auto bind() -> void {
		in_bound = true;
		bindable.bind();
		top.unbind();
	}
	auto unbind() -> void {
		in_bound = false;
		bindable.unbind();
		top.bind();
	}
	explicit ScopedBind(T &bindable, Y &top) : bindable(bindable), top(top) { bind(); }
	~ScopedBind() {
		if(in_bound){
			unbind();
		}
	}
	ScopedBind(ScopedBind &) = delete;
	ScopedBind(ScopedBind && other) noexcept :bindable(other.bindable),top(other.top),in_bound(other.in_bound){
		other.in_bound = false;
	}

	[[nodiscard]] auto is_bound() const noexcept -> bool { return in_bound; }


	bool in_bound = false;

	T &bindable;
	Y &top;
};

} // namespace kczi
