#include "video_init.hpp"
#include <GL/glew.h>
#include <SDL2/SDL_video.h>
#include <string_view>
namespace kczi {
VideoInitErr::VideoInitErr(u8 err) : err(err) {}
auto VideoInitErr::what() const noexcept -> std::string_view {
  if (err == 1) {
    return "Failed to initalize window";
  } else if (err == 2) {
    return "Failed to initalize gl context";
  } else {
    return "ok";
  }
}
auto VideoInitErr::operator==(const VideoInitErr &other) const noexcept
    -> bool {
  return other.err == err;
}
GlContext::GlContext(GlContext &&other) noexcept
    : window(other.window), sld_gl_context(other.sld_gl_context){
  other.window = nullptr;
  other.sld_gl_context = nullptr;
}
GlContext::~GlContext() {
  SDL_DestroyWindow(window);
  window = nullptr;
  sld_gl_context = nullptr;
}
auto GlContext::swap() noexcept -> void { SDL_GL_SwapWindow(window); }
auto init_viedo(const GlContext::Params &params)
    -> Expected<GlContext, VideoInitErr> {
  GlContext context{};
  context.window = SDL_CreateWindow(
      params.window_name.c_str(), SDL_WINDOWPOS_UNDEFINED,
      SDL_WINDOWPOS_UNDEFINED, params.window_size.x, params.window_size.y,
      SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN);
  if (!context.window) [[unlikely]] {
    return (VideoInitErr(1));
  }
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

  context.sld_gl_context = SDL_GL_CreateContext(context.window);
  if (!context.sld_gl_context) [[unlikely]] {
    return (VideoInitErr(2));
  }
  glewExperimental = true;
  glewInit();
  SDL_GL_SetSwapInterval(0);
  return context;
}
} // namespace kczi