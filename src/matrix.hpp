#pragma once

#include "types.hpp"
#include "pch.hpp"

namespace kczi {
using MatrixData = std::array<float, 16>;
class Matrix {
public:
  Matrix();
  ~Matrix() = default;
  explicit Matrix(f2_32 end, f2_32 start = {0.0f, 0.0f});
  MatrixData matrix_data{};
};
} // namespace kczi