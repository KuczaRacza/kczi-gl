#pragma once

#include "pch.hpp"
namespace kczi {
class Renderer;
class Scissors {
public:
	explicit Scissors(Renderer &renderer);
	~Scissors() = default;
	u4_32 rect{};
	auto bind() const noexcept -> void;
	auto unbind() noexcept -> void;
	Renderer &renderer;
};
class Viewport {
public:
	explicit Viewport(Renderer &renderer);
	~Viewport() = default;
	u4_32 rect{};

	auto bind() const noexcept -> void;
	auto unbind() noexcept -> void;
	Renderer &renderer;
};
} // namespace kczi