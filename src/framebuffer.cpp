#include "framebuffer.hpp"
#include <GL/glew.h>
namespace kczi {
Framebuffer::Framebuffer(u2_32 size, Renderer &renderer) : renderer(renderer), fb_viewport(renderer) {
	create(size);
}
auto Framebuffer::create(u2_32 m_size) -> void {
	this->size = m_size;
	texture.create(m_size, true);
	glGenFramebuffers(1, &handle);
	glBindFramebuffer(GL_FRAMEBUFFER, handle);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, texture.handle, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
Framebuffer::Framebuffer(Framebuffer &&other) noexcept : size(other.size), handle(other.handle), texture(std::move(other.texture)), renderer(other.renderer), fb_viewport(other.fb_viewport) {
	other.handle = 0;
	other.size = {0u, 0u};
}
Framebuffer::~Framebuffer() {
	glDeleteFramebuffers(1, &handle);
	handle = 0;
	size = {0u, 0u};
}
auto Framebuffer::operator=(Framebuffer &&other) noexcept -> Framebuffer & {
	size = other.size;
	handle = other.handle;
	texture = std::move(other.texture);
	other.handle = 0;
	other.size = {0u, 0u};
	return *this;
}
auto Framebuffer::bind() const noexcept -> void {

	glBindFramebuffer(GL_FRAMEBUFFER, handle);
}
auto Framebuffer::unbind() noexcept -> void {
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}
auto Framebuffer::clear() const noexcept -> void {
	bind();
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT);
}
Framebuffer::Framebuffer(Renderer &renderer) : renderer(renderer), fb_viewport(renderer) {
}

} // namespace kczi