#include "texture.hpp"
#include <GL/glew.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_surface.h>
#include <stdexcept>
#include <utility>
namespace kczi {
Texture::Texture(Texture &&other) noexcept : handle{std::exchange(other.handle, 0)}, size(other.size) {
	other.size = {0u, 0u};
}

auto Texture::operator=(Texture &&other) noexcept -> Texture & {
	handle = std::exchange(other.handle, 0);
	size = other.size;
	other.size = {0u, 0u};
	return *this;
}
auto Texture::create(u2_32 m_size, bool m_alpha, Filter filter) -> void {
	this->size = m_size;
	glGenTextures(1, &handle);
	bind();
	glTexImage2D(GL_TEXTURE_2D, 0, (m_alpha) ? GL_RGBA : GL_RGB, (i32)m_size.x, (i32)m_size.y, 0, (m_alpha) ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, (GLint)filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, (GLint)filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}
Texture::Texture(u2_32 size, bool alpha, Filter filter) {
	this->alpha = alpha;
	create(size, alpha, filter);
}
Texture::~Texture() {
	glDeleteTextures(1, &handle);
	handle = 0;
	size = {0u, 0u};
}
auto Texture::bind() const noexcept -> void {
	glBindTexture(GL_TEXTURE_2D, handle);
}
auto Texture::load(const std::span<const u8> &data, u2_32 m_size, bool m_alpha) noexcept -> void {
	bind();
	alpha = m_alpha;
	size = m_size;
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, (i32)m_size.x, (i32)m_size.y, (m_alpha) ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data.data());
}
auto Texture::load(const std::string_view &path) -> void {
	SDL_Surface *surface = IMG_Load(std::string(path).c_str());
	if (surface == nullptr) [[unlikely]] {
		throw std::runtime_error("Failed to load image");
	}
	if (surface->format->BytesPerPixel == 4) {
		alpha = true;
	} else {
		alpha = false;
	}
	size = {u32(surface->w), u32(surface->h)};
	create(size, alpha);
	load(std::span<const u8>((u8 *)surface->pixels, surface->h * surface->pitch), size, alpha);
	SDL_FreeSurface(surface);
}
} // namespace kczi