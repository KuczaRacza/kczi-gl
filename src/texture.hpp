#pragma once
#include "pch.hpp"
#include <scoped_bind.hpp>
#include <string_view>
namespace kczi {

class Texture {
public:
	enum class Filter {
		Nearest = GL_NEAREST,
		Linear = GL_LINEAR,
	};
	Texture() = default;
	Texture(Texture &) = delete;
	Texture(Texture &&) noexcept;
	explicit Texture(u2_32 size, bool alpha = true, Filter filter = Filter::Linear);
	~Texture();
	auto operator=(Texture &&) noexcept -> Texture &;
	auto operator=(Texture &) -> Texture & = delete;
	auto create(u2_32 m_size, bool m_alpha, Filter filter = Filter::Linear) -> void;
	u2_32 size{};
	u32 handle{};
	bool alpha = true;
	auto bind() const noexcept -> void;
	auto load(const std::span<const u8> &data, u2_32 m_size, bool m_alpha) noexcept -> void;
	auto load(const std::string_view &path) -> void;
};

} // namespace kczi
