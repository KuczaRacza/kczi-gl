#pragma once
#include "pch.hpp"
#include "scoped_bind.hpp"
#include "scissors.hpp"
namespace kczi {
class Renderer {
public:
	struct Params{
		bool depth_test = false;
		bool cull_face = false;
		bool blend = true;
		bool scissor_test = false;
		f4_32 color= {1.0f, 1.0f, 1.0f, 1.0f};
	} params;
	explicit Renderer(const Params & create_params);
	Renderer(Renderer &&) = default;
	~Renderer() =default;
	Renderer(const Renderer &) = delete;
	[[no_unique_address]] DummyBind dummy_bind;
	Scissors scissors;
	Viewport viewport;
	ScopedBind<Scissors, DummyBind> scissors_bind{scissors, dummy_bind};
	ScopedBind<Viewport, DummyBind> viewport_bind{viewport, dummy_bind};
	const Viewport * current_viewport = nullptr;
	const Scissors * current_scissors = nullptr;
	bool scissors_enabled = false;
	auto enable_scissors() noexcept -> void;
	auto disable_scissors() noexcept -> void;
	auto clear() noexcept -> void;
};
} // namespace kczi