#pragma once
#include "pch.hpp"
#include <bits/utility.h>
#include <cstddef>
#include <stdexcept>
#include <tuple>
namespace kczi {

template <typename T>
concept VertexType = requires(T obj) {
											 std::is_trivially_copyable<T>();
											 T::size_elements();
											 T::stride();
											 T::template pointer<0>();
											 T::template attrib_size<0>();
										 };
namespace VAO {
enum class CoordType : u8 {
	NONE,
	X,
	XY,
	XYZ,
};
enum class ColorType : u8 {
	NONE,
	RGB,
	RGBA,
	BW,
};
enum class UV : u8 {
	NONE,
	UV,
};
constexpr u32 element_number(CoordType coord, ColorType color, UV uv) {
	u32 elements = 0;
	if (coord != CoordType::NONE) {
		elements++;
	}
	if (uv != UV::NONE) {
		elements++;
	}
	if (color != ColorType::NONE) {
		elements++;
	}
	return elements;
}
constexpr u32 attrib_size(CoordType coord) { return (u32)coord; }
constexpr u32 attrib_size(ColorType color) {
	switch (color) {
	case ColorType::BW:
		return 1;
	case ColorType::RGB:
		return 3;
	case ColorType::RGBA:
		return 4;
	case ColorType::NONE:
		return 0;
	}
}
constexpr u32 attrib_size(UV uv) { return (uv == UV::UV) ? 2 : 0; }

constexpr u32 attrib_size(CoordType coord, ColorType color, UV uv) {
	return (attrib_size(coord) + attrib_size(color) + attrib_size(uv));
}
constexpr u32 stride(CoordType coord, ColorType color, UV uv) {
	return (attrib_size(coord) + attrib_size(color) + attrib_size(uv)) *
				 sizeof(f32);
}

constexpr u32 pointer(CoordType coord, ColorType color, u32 element) {
	switch (element) {
	case 0:
		return 0;
	case 1:
		return attrib_size(coord) * sizeof(f32);
	case 2:
		return (attrib_size(coord) + attrib_size(color)) * sizeof(f32);
	default:
		return 0;
	}
}
constexpr u32 attrib_size(CoordType coord, ColorType color, UV uv,
													u32 element) {
	std::vector<u32> sizes;
	auto add = [&](auto type) {
		if (attrib_size(type) != 0) {
			sizes.push_back(attrib_size(type));
		}
	};
	add(coord);
	add(color);
	add(uv);
	return sizes[element];
}

} // namespace VAO

template <VAO::CoordType coord_type = VAO::CoordType::XY,
					VAO::ColorType color_type = VAO::ColorType::RGBA,
					VAO::UV uv_type = VAO::UV::NONE>
class Vertex {
public:
	static constexpr auto m_size_elements =
			VAO::element_number(coord_type, color_type, uv_type);
	std::array<f32, VAO::attrib_size(coord_type, color_type, uv_type)> m_data;

	static constexpr auto size_elements() -> u32 { return m_size_elements; }
	template <size_t E>
	static constexpr auto pointer() -> u32 {
		return VAO::pointer(coord_type, color_type, E);
	}
	template <size_t E>
	static constexpr auto attrib_size() -> u32 {
		return VAO::attrib_size(coord_type, color_type, uv_type, E);
	}
	static constexpr auto stride() -> u32 {
		return VAO::stride(coord_type, color_type, uv_type);
	}
};

template <VertexType vertex_type, u32 i = 0>
auto enable_attribs_template() -> void {
	constexpr auto attrib_size = vertex_type::template attrib_size<i>();
	constexpr auto stride = vertex_type::stride();
	constexpr auto pointer = vertex_type::template pointer<i>();
	glVertexAttribPointer(i, attrib_size, GL_FLOAT, GL_FALSE, stride,
												(GLvoid *)pointer);
	if constexpr (i + 1 != vertex_type::size_elements()) {
		return enable_attribs_template<vertex_type, i + 1>();
	}
}
template <VertexType vertex_type>
class VertexArray {
	void generte_hadles() {
		glGenVertexArrays(1, (GLuint *)&handle_id);
		glBindVertexArray((GLuint)handle_id);
		glGenBuffers(1, &buffer_handle_id);
		glBindBuffer(GL_ARRAY_BUFFER, buffer_handle_id);
	}
	void enableAttrib() {
		enable_attribs_template<vertex_type>();
		for (u32 i = 0; i < vertex_type::size_elements(); i++) {
			glEnableVertexAttribArray(i);
		}
	}

public:
	explicit VertexArray(const std::span<const vertex_type> &data) : size(data.size()) {
		generte_hadles();
		glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(vertex_type),
								 (void *)data.data(), GL_DYNAMIC_DRAW);
		enableAttrib();
		u32 err = glGetError();
		if (err != GL_NO_ERROR) {
			throw std::runtime_error("ERROR: Could not create a VAO");
		}
	}
	explicit VertexArray(size_t size) : size(size) {
		generte_hadles();
		glBufferData(GL_ARRAY_BUFFER, size * sizeof(vertex_type), NULL,
								 GL_DYNAMIC_DRAW);
		enableAttrib();
		u32 err = glGetError();
		if (err != GL_NO_ERROR) {
			throw std::runtime_error("ERROR: Could not create a VAO");
		}
	}
	VertexArray(VertexArray &&other) noexcept : handle_id(other.handle_id), buffer_handle_id(other.buffer_handle_id),
																							size(other.size) {
		other.handle_id = 0;
		other.size = 0;
		other.buffer_handle_id = 0;
	}
	VertexArray(VertexArray &) = delete;
	VertexArray() = delete;
	~VertexArray() {
		if (handle_id != 0) {
			glDeleteVertexArrays(1, &handle_id);
			glDeleteBuffers(1, &buffer_handle_id);
		}
	}
	auto bind() -> void { glBindVertexArray(handle_id); }
	auto draw() -> void {
		bind();
		glDrawArrays(GL_TRIANGLES, 0, size);
	}

	u32 handle_id = 0;
	u32 buffer_handle_id = 0;
	u32 size = 0;
};

} // namespace kczi