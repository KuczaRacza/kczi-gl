#include "shader.hpp"
#include <cstddef>
#include <expected.hpp>
#include <stdexcept>
#include "pch.hpp"
namespace kczi {

auto GlProgramError::what() const noexcept -> std::string_view {
  if (err == 1) {
    return compile_error;
  } else if (err == 2) {
    return "Cannot link program";
  } else if (err == 3) {
    return "Cannot create program";
  } else {
    return "Unknown error";
  }
}
ShaderRaw::ShaderRaw(ShaderRaw &&other) noexcept : handle_id(other.handle_id) {
  other.handle_id = 0;

}
ShaderRaw::~ShaderRaw() { glDeleteProgram((GLuint)handle_id); }
ShaderRaw::ShaderRaw() = default;
template <GLenum type>
static inline auto compileShader(const std::string &source)
    -> Expected<u32, GlProgramError> {
  u32 handle = glCreateShader(type);
  const char *c_source = source.c_str();
  glShaderSource(handle, 1, &c_source, NULL);
  glCompileShader(handle);
  GLint sucess = 0;
  glGetShaderiv(handle, GL_COMPILE_STATUS, &sucess);
  if (sucess == GL_FALSE) [[unlikely]] {
    GlProgramError err;
    i32 log_size;
    glGetShaderiv(handle, GL_INFO_LOG_LENGTH, (GLint *)&log_size);
    err.compile_error.resize(log_size);
    err.compile_error += "\nSOURCE:\n";
    err.compile_error += source;
    glGetShaderInfoLog(handle, log_size, NULL, err.compile_error.data());
    return (err);
  }
  return (handle);
}
auto ShaderRaw::bind() const -> void { glUseProgram(handle_id); }

auto create_program(const std::string &fs_source, const std::string &vs_source)
    -> ShaderRaw {
  ShaderRaw prog;
  GlProgramError err{};

  u32 fs_handle;
  u32 vs_handle;

  vs_handle = compileShader<GL_VERTEX_SHADER>(vs_source).get();
  fs_handle = compileShader<GL_FRAGMENT_SHADER>(fs_source).get();

  prog.handle_id = glCreateProgram();

  glAttachShader(prog.handle_id, vs_handle);
  glAttachShader(prog.handle_id, fs_handle);

  glLinkProgram(prog.handle_id);

  glDetachShader(prog.handle_id, vs_handle);
  glDetachShader(prog.handle_id, fs_handle);

  glDeleteShader(fs_handle);
  glDeleteShader(vs_handle);

  u32 gl_err = glGetError();
  if (gl_err != GL_NO_ERROR) [[unlikely]] {
    throw std::runtime_error("Cannot link program");
  }
  return prog;
}

} // namespace kczi