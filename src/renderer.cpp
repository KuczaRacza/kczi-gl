#include "renderer.hpp"
#include "types.hpp"
#include <GL/glew.h>

namespace kczi {
Renderer::Renderer(const Params & creation_params): scissors(*this), viewport(*this) {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	if(params.depth_test) {
		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LESS);
	}
	if(params.cull_face) {
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);
		glFrontFace(GL_CCW);

	}

}
auto Renderer::enable_scissors()noexcept -> void {
	scissors_enabled = true;
	glEnable(GL_SCISSOR_TEST);
}
auto Renderer::disable_scissors() noexcept-> void {
	glDisable(GL_SCISSOR_TEST);
	scissors_enabled = false;
}
auto Renderer::clear() noexcept -> void {
	glClearColor(params.color.x, params.color.y, params.color.w, params.color.h);
	glClear(GL_COLOR_BUFFER_BIT | (params.depth_test ? GL_DEPTH_BUFFER_BIT : 0) | (params.cull_face ? GL_STENCIL_BUFFER_BIT : 0));
}

} // namespace kczi