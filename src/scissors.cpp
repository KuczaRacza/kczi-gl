#include "scissors.hpp"
#include "renderer.hpp"
#include <GL/glew.h>
namespace kczi {
auto Scissors::bind() const noexcept -> void {
	glScissor((i32)rect.x, (i32)rect.y, (i32)rect.h, (i32)rect.w);
	renderer.current_scissors = this;
}
auto Scissors::unbind() noexcept -> void {
	renderer.current_scissors = nullptr;
}
Scissors::Scissors(Renderer &renderer):renderer(renderer) {

}
auto Viewport::bind() const noexcept -> void {
	glViewport((i32)rect.x, (i32)rect.y, (i32)rect.h, (i32)rect.w);
	renderer.current_viewport = this;
}
auto Viewport::unbind() noexcept -> void {
	renderer.current_viewport = nullptr;
}
Viewport::Viewport(Renderer &renderer):renderer(renderer) {

}
} // namespace kczi